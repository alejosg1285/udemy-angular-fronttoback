import { Injectable } from '@angular/core';
// import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Client } from '../models/Client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clients: Observable<any[]>;
  // client: FirebaseObjectObservable<any>;

  constructor(public af: AngularFirestore) {
    // this.clients = this.af.list('/clients') as FirebaseListObservable<Client[]>;
    this.clients = this.af.collection('clients').valueChanges();
  }

  getClients() {
    return this.clients;
  }
}
