import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Client } from '../models/Client';

@Injectable({
    providedIn: 'root',
})
export class FirebaseService {
    constructor(private afs: AngularFirestore) {}

    getClients() {
        /*return new Promise<any>((resolve, reject) => {
            this.afs.collection('/clients').snapshotChanges()
            .pipe(
                map(actions => actions.map(a => ({...a})))
            )
            .subscribe(snapshots => {
                resolve(snapshots);
            });
        });*/
        return this.afs.collection('clients').snapshotChanges();
    }

    newClient(value: Client) {
        return this.afs.collection('clients').add({
            firstName: value.firstName,
            lastName: value.lastName,
            email: value.email,
            phone: value.phone,
            balance: value.balance
        });
    }

    getClient(id: string) {
        return this.afs.collection('clients').doc(id).snapshotChanges();
    }

    updateClient(id: string, client: Client) {
        console.log(client);
        return this.afs.collection('clients').doc(id).set(client);
    }

    deleteClient(id: string) {
        return this.afs.collection('clients').doc(id).delete();
    }
}
