import { Component, OnInit } from '@angular/core';
// import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  clients: Array<any>;
  totalOwed: number;

  constructor(public firebaseService: FirebaseService) {}

  ngOnInit() {
    this.firebaseService.getClients().subscribe(result => {
      this.clients = result;

      this.getTotalOwed();
    });
  }

  getTotalOwed(): void {
    let total = 0;

    for (let i = 0; i < this.clients.length; i++) {
      total += parseFloat(this.clients[i].payload.doc.data().balance);
    }

    this.totalOwed = total;
  }
}
