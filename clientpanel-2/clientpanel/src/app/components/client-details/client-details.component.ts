import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Client } from '../../models/Client';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit {
  id: string;
  client: any;
  objClient: Client;
  hasBalance = false;
  showBalanceUpdateInput = false;

  constructor(public firebaseService: FirebaseService, public flashMessagesService: FlashMessagesService, public router: Router, public route: ActivatedRoute) {}

  ngOnInit() {
    // Get id
    this.id = this.route.snapshot.params['id'];

    this.objClient = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      balance: 0
    };

    // Get client.
    this.firebaseService.getClient(this.id).subscribe(client => {
      this.client = client;

      const c: Client = {
        balance: this.client.payload.data().balance,
        firstName: this.client.payload.data().firstName,
        lastName: this.client.payload.data().lastName,
        email: this.client.payload.data().email,
        phone: this.client.payload.data().phone
      };

      if (this.client.payload.data().balance > 0) {
        this.hasBalance = true;
      }

      this.objClient = c;
    });
  }

  updateBalance(id: string) {
    this.firebaseService.updateClient(id, this.objClient);
    this.flashMessagesService.show('Balance updates', { cssClass: 'alert-success', timeout: 4000 });
    this.router.navigate(['/client/' + this.id]);
  }

  onDeleteClick() {
    if (confirm('Are you sure to delete?')) {
      this.firebaseService.deleteClient(this.id);
      this.flashMessagesService.show('Client deleted', { cssClass: 'alert-success', timeout: 4000 });
      this.router.navigate(['/']);
    }
  }
}
