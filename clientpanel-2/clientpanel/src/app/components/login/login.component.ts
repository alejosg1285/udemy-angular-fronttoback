import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(public flashMessagesService: FlashMessagesService, public router: Router, public authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.login(this.email, this.password)
    .then(res => {
      this.flashMessagesService.show('You are loggin in', { cssClass: 'alert-success', timeout: 4000 });
      this.router.navigate(['/']);
    })
    .catch(err => {
      this.flashMessagesService.show(err.messages, { cssClass: 'alert-danger', timeout: 4000 });
      this.router.navigate(['/login']);
    });
  }
}
