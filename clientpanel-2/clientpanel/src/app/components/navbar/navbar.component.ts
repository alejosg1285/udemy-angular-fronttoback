import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isLogginIn: boolean;
  loggedInUser: string;
  showRegister: boolean;

  constructor(public flashMessagesService: FlashMessagesService,
    public router: Router,
    public authService: AuthService,
    public settingsService: SettingsService) { }

  ngOnInit() {
    this.showRegister = this.settingsService.getSettings().allowRegistration;

    this.authService.getAuth().subscribe(auth => {
      if (auth) {
        this.isLogginIn = true;
        this.loggedInUser = auth.email;
      } else {
        this.isLogginIn = false;
      }
    });
  }

  onLogoutClick() {
    this.authService.logout();
    this.flashMessagesService.show('You are logged out', { cssClass: 'alert-success', timeput: 4000 });
    this.router.navigate(['/login']);
  }
}
